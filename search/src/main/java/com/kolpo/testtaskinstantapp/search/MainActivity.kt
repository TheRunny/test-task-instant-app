package com.kolpo.testtaskinstantapp.search

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        web_view.settings.javaScriptEnabled = true

        web_view.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                val visibility = progress_bar.visibility
                when (progress) {
                    100 -> progress_bar.visibility = View.INVISIBLE
                    else -> {
                        if (visibility == View.INVISIBLE) {
                            progress_bar.visibility = View.VISIBLE
                        }
                    }
                }
                progress_bar.progress = progress
            }
        }

        web_view.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) view?.loadUrl(replaceToHttps(url))
                return false
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(replaceToHttp(failingUrl)))
                startActivity(browserIntent)
            }
        }

        search_input.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                val url = replaceToHttps(search_input.text.toString())
                web_view.loadUrl(url)
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun replaceToHttps(url: String): String {
        var result: String = url.replace(HTTP, HTTPS)
        if (!result.startsWith(HTTP) && !result.startsWith(HTTPS)) {
            result = HTTPS + url
        }
        return result
    }

    private fun replaceToHttp(url: String): String {
        var result: String = url.replace(HTTPS, HTTP)
        if (!result.startsWith(HTTP) && !result.startsWith(HTTPS)) {
            result = HTTP + url
        }
        return result
    }

    companion object {
        const val HTTP: String = "http://"
        const val HTTPS: String = "https://"
    }
}
